---
layout: post
status: publish
published: true
title: Writing Testable Web API Wrappers
author:
  display_name: Martin Doms
  login: Martin Doms
  email: contact@martindoms.com
  url: http://martindoms.com
author_login: Martin Doms
author_email: contact@martindoms.com
author_url: http://martindoms.com
wordpress_id: 377
wordpress_url: http://blog.martindoms.com/?p=377
date: '2011-01-16 21:17:24 +1100'
date_gmt: '2011-01-16 08:17:24 +1100'
---
<p>Writing a wrapper for a web API seems like a pretty simple thing to do. I mean, most of the code is already written for you, all you are doing is adapting the response from some web sever into your own object model. In most cases your object model looks very similar to the server's web service representation, so what's difficult about this?</p>
<p>In fact there are several pitfalls and architectural decisions to be made when writing web API wrappers. I intend to write about some of these in the coming months, but today I'm going to talk about writing web API wrappers in a testable way, following a <a href="http://en.wikipedia.org/wiki/Test-driven_development">Test-Driven Development</a> (TDD) approach to the problem. This basically means that before I write a lick of code, I must first write the tests to exercise the code.</p>
<p>My sample application is going to be very simple, with a single API method and a single test, against the Flickr API. It is not designed as a demonstration of how to structure an API wrapper, it is merely for the purposes of demonstrating the principles of this article.</p>
<p>Before we jump into the code, let's carefully consider exactly what it is we want to test. Are we testing that the Flickr API gives us an XML response that looks like what we expect, given a valid input? <b>NO!!</b> We approach this problem under the assumption that the Flickr team has already thoroughly tested their API, and that given a valid input we <i>will</i> get the correct output. Do we test that our HTTP layer is in fact able to connect to the Flickr service and send a request? Again, <b>no!</b> Presumable whoever wrote the HTTP library we are using has tested this. </p>
<p>In preparing for this article I looked at a ton of REST API wrapper projects in the .NET, Ruby and Python communities, wrapping Twitter, Flickr, Chargify and others. Nearly all of them run "unit tests" that involve actual communication with the servers they are working against. <i>This is not a unit test!</i> These end-to-end tests are called 'integration tests' (or if they are very broad, 'smoke tests'). They are valuable, but they are not unit tests. Unit tests need to be very fast and very reliable to run, in order to ensure developers run them frequently. Testing over a network connection and a server half way across the world is neither fast nor reliable, particularly when running hundreds of tests at a time.</p>
<p>So now that we have that rant out of the way, let's look at how we structure our API wrapper in such a way that we can test it independently of the API itself. As I said, we're using the Flickr API and I'm going to be calling a method that returns the public 'favorites' for a particular User ID, <i>flickr.favorites.getPublicList</i>. I'll be working in C# with the Visual Studio testing tools, but the principles here apply to almost any language and framework.<br />
[caption id="attachment_381" align="alignright" width="300" caption="Click to enlarge"]<a href="/images/2011-01-apiblog01.png"><img src="/images/2011-01-apiblog01-300x216.png" alt="" title="apiblog01" width="300" height="216" class="size-medium wp-image-381" /></a>[/caption]<br />
First I create a new solution with a C# class library project and a unit test project. Don't forget to reference the API project from the unit test project. After I delete the goo that comes with new projects and create a couple of my own classes my project looks like the image on the right.</p>
<p>Now we need to write the test. This has to happen before we write any code if we are following TDD principles. Let's consider exactly what we want to test. We're going to test a method on the Flickr class that should return a list of photos given a user ID. We are <i>not</i> going to test the HTTP layer or the service. This means that we need to mock up the service/HTTP stuff. Here is the code for our test:</p>
<p>{% highlight csharp %}

using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestableApi;
using Moq;
using System.Xml.Linq;
namespace UnitTests
{
    [TestClass]
    public class FlickrTests
    {
        private const string ApiKey = &quot;[My API key]&quot;;
        [TestMethod]
        public void GetFavouritesTest()
        {
            // Arrange
            string userId = &quot;22860405@N02&quot;;
            string url = &quot;http://api.flickr.com/services/rest/&quot;;
            var args = new List<KeyValuePair<string,string>>
            {
                new KeyValuePair<string,string>(&quot;method&quot;, &quot;flickr.favorites.getPublicList&quot;),
                new KeyValuePair<string,string>(&quot;api_key&quot;, ApiKey),
                new KeyValuePair<string,string>(&quot;user_id&quot;,userId)
            };
            var helper = new Mock<IWebHelper>();
            helper.Setup(q => q.HttpGet(url, args)).
                Returns(GetFavouritesResponse);
            Flickr flickr = new Flickr(helper.Object);
            // Act
            var result = flickr.GetFavouritesFor(userId);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result.First().Id);
        }
        // Helpers
        private string GetFavouritesResponse
        {
            get
            {
                return new XDocument(
                    new XDeclaration(&quot;1.0&quot;, &quot;UTF-8&quot;, &quot;yes&quot;),
                    new XElement(&quot;photos&quot;,
                        new XAttribute(&quot;page&quot;, &quot;1&quot;),
                        new XAttribute(&quot;pages&quot;, &quot;1&quot;),
                        new XAttribute(&quot;perpage&quot;, &quot;100&quot;),
                        new XAttribute(&quot;total&quot;, &quot;1&quot;),
                        new XElement(&quot;photo&quot;,
                            new XAttribute(&quot;id&quot;, &quot;1&quot;),
                            new XAttribute(&quot;owner&quot;, &quot;22860405@N02&quot;),
                            new XAttribute(&quot;secret&quot;, &quot;abc&quot;),
                            new XAttribute(&quot;server&quot;, &quot;123&quot;),
                            new XAttribute(&quot;farm&quot;, &quot;1&quot;),
                            new XAttribute(&quot;title&quot;, &quot;title&quot;),
                            new XAttribute(&quot;date_faved&quot;, &quot;1200000000&quot;)))).ToString();
            }
        }
    }
}

{% endhighlight %}
</p>
<p>I know this looks like a bit much for a unit test, but it's really quite simple.</p>
<h4>Arrange</h4>
<p>This is where most of the action is happening. I'm using the <a href="http://code.google.com/p/moq/wiki/QuickStart">Moq</a> framework for mocking objects. If you haven't used this before then it may look a little confusing. But read at the first few lines of GetFavouritesTest() like this: <i>Given valid user ID, API endpoint URL and API arguments, my mock implementation of the IWebHelper interface should return valid XML</i>. This is how we mock the service - we assume that given the right input we will get the expected output. The GetFavouritesResponse property is just some XML we would expect the server to send us given these arguments. So what we're really testing is our class's ability to transform this response into something useful, in this case a collection of Photo objects.</p>
<h4>Act</h4>
<p>This is nice and simple, we just call the method and store the output in our result variable.</p>
<h4>Assert</h4>
<p>And here we just check that the output is what we would expect, given the input.</p>
<p>So now we get to writing the actual code. I'm going to start with the Photo class, just to get it out of the way. Note that while doing true TDD you will be writing a test for Photo as well, before you create this Photo class. In this demonstration I'm going to skip this, and give a simplified version of Photo.</p>
<p>{% highlight csharp %}

using System;
using System.Xml.Linq;
namespace TestableApi
{
    public class Photo
    {
        public Photo(XElement xmlNode)
        {
            Id = Convert.ToInt32(xmlNode.Attribute(&quot;id&quot;).Value);
            // other properties from XML
        }
        public int Id { get; set; }
        // all other Photo fields
    }
}

{% endhighlight %}
</p>
<p>The Photo class takes an XElement argument and sets the properties as needed. Whether this is a good design or not is up for debate and is something I will be discussing in a future blog post, but it's quick and it works, so it fits perfectly with the TDD mantra, <a href="http://jamesshore.com/Blog/Red-Green-Refactor.html">Red-Green-Refactor</a>.</p>
<p>Next we're going to look at the IWebHelper interface.</p>
<p>{% highlight csharp %}

using System.Collections.Generic;
namespace TestableApi
{
    public interface IWebHelper
    {
        string HttpGet(string url, IEnumerable<KeyValuePair<string,string>> args);
    }
}

{% endhighlight %}
</p>
<p>In a proper system we would also have a POST method, but this works for now. Finally, Flickr class. For now we write just enough code to allow us to compile and run the test. We are expecting it to fail.</p>
<p>{% highlight csharp %}
using System.Collections.Generic;
using System.Xml.Linq;
namespace TestableApi
{
    public class Flickr
    {
        private readonly IWebHelper webHelper;
        public Flickr(IWebHelper webHelper)
        {
            this.webHelper = webHelper;
        }
        public ICollection<Photo> GetFavouritesFor(string userid)
        {
            return null;
        }
    }
}

{% endhighlight %}
</p>
<p>Notice how we are using <a href="http://en.wikipedia.org/wiki/Dependency_injection">dependency injection</a> (in this case, constructor injection) to allow consumers of the API to inject their own implementations of our dependencies through the constructor. This is crucial to making your libraries testable. Without the ability to inject dependencies we are simply unable to provide mock services.</p>
<p>Now we run our tests, and as expected:<br />
<a href="/images/2011-01-fail.png"><img src="/images/2011-01-fail.png" alt="" title="fail" width="602" height="47" class="alignnone size-full wp-image-393" /></a></p>
<p>Cool! We have a failing test. Now it's time to get to work in making it pass. Remember, in TDD we write <i>just enough code to pass the tests</i>, then <b>STOP!</b> In my opinion this discipline leads us to simple, easy-to-read code and saves lots of time in fluffing about writing code that is never fully exercised. I'll just go ahead and paste in the complete Flickr class:</p>
<p>{% highlight csharp %}

using System.Collections.Generic;
using System.Xml.Linq;
namespace TestableApi
{
    public class Flickr
    {
        private const string ApiKey = &quot;[My API Key]&quot;;
        private readonly IWebHelper webHelper;
        // Uncomment this when we create and test the real concrete
        // WebHelper
        //public Flickr() : this(new WebHelper()){ }
        internal Flickr(IWebHelper webHelper)
        {
            this.webHelper = webHelper;
        }
        public ICollection<Photo> GetFavouritesFor(string userid)
        {
            var result = new List<Photo>();
            var args = new List<KeyValuePair<string,string>>();
            args.Add(new KeyValuePair<string,string>(&quot;method&quot;, &quot;flickr.favorites.getPublicList&quot;));
            args.Add(new KeyValuePair<string,string>(&quot;api_key&quot;, ApiKey));
            args.Add(new KeyValuePair<string,string>(&quot;user_id&quot;, userid));
            string resp = webHelper.HttpGet(&quot;http://api.flickr.com/services/rest/&quot;, args);
            var doc = XDocument.Parse(resp);
            foreach (var node in doc.Element(&quot;photos&quot;).Elements(&quot;photo&quot;))
                result.Add(new Photo(node));
            return result;
        }
    }
}

{% endhighlight %}
</p>
<p>A quick note about the constructors: notice how I made the injectable constructor <i>internal</i>. Whether you do this or not is up to you and will depend on the nature of your project and your dependencies. In this case, I don't want to expose the IWebHelper interface to my library users and they will have no need to use this constructor. Clever readers may have noticed that the IWebHelper interface is publicly exposed. I have to do this so that the Moq mocking framework will be able to mock it in the test. If someone knows a way around this so that I can keep IWebHelper internal, I'd love to hear it. Also if you're using C#, to make internal members available to the unit test project, simply add this line to your project's AssmbleInfo.cs file:<br />
{% highlight csharp %}
[assembly: InternalsVisibleTo(&quot;UnitTests&quot;)]
{% endhighlight %}
(Replace "UnitTests" with your unit test project's name).</p>
<p>Now when we run the tests:<br />
<a href="/images/2011-01-pass.png"><img src="/images/2011-01-pass.png" alt="" title="pass" width="564" height="48" class="alignnone size-full wp-image-396" /></a><br />
:)</p>
<h3>Summary and Remarks</h3>
<p>First, a couple of remarks on this code. Yes, there absolutely should be more unit tests in this code. Obviously this is a demonstration project, so take it for what it is - not production code. Some people may also take issue with the granularity of the unit test I provided. This test is technically exercising more than one functional unit of work. It tests that our method actually calls into the web helper class, and it also tests that it handles the output of this class appropriately. I absolutely agree that this should be refactored and broken into more tests. The purpose of this article was to show how a project can be structured in such a way that it is testable, so again, take it for what it is.</p>
<p>To summarize, here are the steps we took to make our project nicely unit testable.</p>
<ul>
<li>Identify exactly which parts of the system we want to test, and which parts we don't</li>
<li>Where we are interacting with parts of the system that are not being tested, hide them behind interfaces</li>
<li>Provide affordance for your class library consumers to inject their own implementations of these dependencies, as seen in the Flickr class constructors above</li>
<li>In the test, mock these interfaces and provide appropriate output from the mock given the input. Don't forget to run tests with mocks that behave correctly given <i>bad</i> input as well as good.</li>
</ul>
<p>Hope this helps, thanks for reading.</p>
