---
layout: post
status: publish
published: true
title: Ray Tracer in F# - Part II
author:
  display_name: Martin Doms
  login: Martin Doms
  email: contact@martindoms.com
  url: http://martindoms.com
author_login: Martin Doms
author_email: contact@martindoms.com
author_url: http://martindoms.com
wordpress_id: 486
wordpress_url: http://blog.martindoms.com/?p=486
date: '2011-02-08 22:55:17 +1100'
date_gmt: '2011-02-08 09:55:17 +1100'
---
<p>In the <a href="http://blog.martindoms.com/2011/02/01/ray-tracer-in-f-part-i/">previous post</a> we set up the bones of a ray caster and got it rendering a very basic "sphere" with no shading or anything. Today we're going to make the sphere look more like a real sphere by applying a Phong shading model to it.</p>
<p>If we want to shade each pixel on the sphere correctly then we're going to need to get some information about the point of intersection between the ray and the sphere. Before getting into code, we'll just start with the mathematical description of our lighting model. This will be brief and I encourage you to investigate further if you have never done any kind of shading or lighting before.</p>
<p>We are going to be working with three different kinds of lighting:</p>
<ol>
<li><strong>Ambient</strong> - ambient lighting is a general light that applies to every surface in the scene, regardless of orientation, shadows, etc. Ambient light has no source. You can think of ambient light as the base-line or "zero-point" of the scene's lighting. It's the amount of light that a portion of the scene will have when in complete shadow. Although there is no basis for ambient light in physics, without it things just look unrealistic. Perfectly black shadows look wrong.</li>
<li><strong>Diffuse</strong> - When light from a source strikes a surface, the surface reflects that light in the same quantity in every direction (think of this as a surface reflecting photons in perfectly random directions). The strength of a diffuse reflection is dependent on the angle of the surface with the light travel path. If a surface is perfectly perpendicular to the ray of light then the reflection will be total (100%). The reflectivity drops as the surfaces turns its face on an angle to the light, until it reaches 0% when the surface is 90' to the light source (this will be described in more detail soon).</li>
<li><strong>Specular</strong> - Specular lights are those little highlights in a scene, particularly on curved surfaces. Like diffuse reflections, specular highlights are dependent on the angle of the surface, but in a different way. Specular lighting will be described in detail towards the end of this article.</li>
</ol>
<p>The three kinds of light add together to form a fully lighted scene:<br />
<a href="/images/2011-02-lighting1.png"><img src="/images/2011-02-lighting1.png" alt="" title="lighting" width="768" height="180" class="aligncenter size-full wp-image-545" /></a></p>
<p>So let's define a new data type in our program that represents an intersection between a ray and our sphere. For now we're still working under the assumption that the scene will only ever have a single sphere. I find working iteratively like this is a good workflow for F#, especially with F# interactive.</p>
<p>{% highlight csharp %}

type Intersection = { normal:Vector3D; point:Point3D; ray:Ray; sphere:Sphere; t:float }

{% endhighlight %}
</p>
<p>An intersection has a surface normal, a point in space, an associated ray and a <i>t</i> value, which describes how far along the ray the intersection happens. We can just derive <i>t</i> from <i>point</i> (or vice versa) but I find it convenient to store both.</p>
<p>Now we're going to want our castRay function to return Intersection objects instead of true or false. It should be capable of returning multiple intersections for a given ray. A sphere will usually have two intersections with a ray, and it's not difficult to imagine shapes that will have more than two. So we'll return a list of intersections.</p>
<p>In the previous article all we did for the ray-sphere intersection was compute the discriminant of a quadratic, but in order to find the exact point in space of the intersection we'll need to solve the full equation for both solutions (if you don't know the quadratic formula, <a href="http://en.wikipedia.org/wiki/Quadratic_equation#Quadratic_formula">read this</a>). Here is the modified castRay function. I have also included a small utility function which we'll make use of, pointAtTime, which just computes the point of some line given by <strong>p</strong> = <strong>a</strong> + <strong>b</strong>t for a given value of t.</p>
<p>{% highlight csharp %}

/// Get the position of a ray at a given time
let pointAtTime ray time =
    ray.origin + time * ray.direction
let castRay ray (scene:Scene) =
    let s = ray.origin - scene.sphere.center
    let rayDir = norm ray.direction
    let sv = Vector3D.DotProduct(s,rayDir)
    let ss = Vector3D.DotProduct(s,s)
    let discr = sv*sv - ss + scene.sphere.radius*scene.sphere.radius
    if discr < 0.0 then []
    else
        let normalAtTime t = norm (pointAtTime ray t - scene.sphere.center)
        let (t1,t2) = (-sv + sqrt(discr), -sv - sqrt(discr))
        [ (t1, { normal = normalAtTime t1; point = pointAtTime ray t1; ray = ray; sphere=scene.sphere });
          (t2, { normal = normalAtTime t2; point = pointAtTime ray t2; ray = ray; sphere=scene.sphere }) ]

{% endhighlight %}
</p>
<p>Just for a sanity check let's modify the guts of the nested for-loop that computes the image just to make sure that what we have got is still producing a correct image.</p>
<p>{% highlight csharp %}

for x in 0..(width-1) do
        for y in 0..(height-1) do
            let rayPoint = vpc + float(x-width/2)*pw*u + float(y-height/2)*ph*v
            let rayDir = norm (rayPoint - scene.camera.position)
            let ray = { origin = scene.camera.position; direction = rayDir }
            match castRay ray scene with
            | [] -> bmp.SetPixel(x,y,Color.Gray)
            | _ -> bmp.SetPixel(x,y,Color.Red)

{% endhighlight %}
</p>
<p>The output is now the same image we were getting at the end of the previous article. Now for some shading. What we need to do is create a function that will transform a list of intersections into the correct colour value. The process for this will be</p>
<ol>
<li>Get intersection with the lowest <i>t</i> value (closest to the camera).</li>
<li>Add the ambient, diffuse and specular lighting components for this ray on this sphere with this surface normal.</li>
<li>Return the aggregate colour value</li>
</ol>
<p>The fact that I can easily break the function into 3 parts like that suggests it shouldn't be a single function, but we are going to refactor later so let's just go for it.</p>
<p>Before we can light something, we need to give it a colour, so let's give our sphere some colour. Because we're going to be doing some operations on colours we'll define our own colour type. The built-in .NET colours don't have affordance for multiplying or adding colours together or multiplying colours by scalar values.</p>
<p>{% highlight csharp %}

type Color(r: float, g: float, b:float) =
    member this.r = r
    member this.g = g
    member this.b = b
    static member ( * ) (c1:Color, c2:Color) =
        Color (c1.r*c2.r, c1.g*c2.g, c1.b*c2.b)
    static member ( * ) (c:Color, s:float) =
        Color (c.r*s, c.g*s, c.b*s)
    static member ( + ) (c1:Color, c2:Color) =
        let r = Math.Min (c1.r+c2.r, 1.0)
        let g = Math.Min (c1.g+c2.g, 1.0)
        let b = Math.Min (c1.b+c2.b, 1.0)
        Color (r,g,b)
    static member Zero = Color(0.0,0.0,0.0)

{% endhighlight %}
</p>
<p>Here we're defining a simple 3-tuple for RGB colour values where 0.0 <= colour <= 1.0. We're defining operations on these types in the same way you would in C# or other OO languages. We've defined a multiplication operation for both scalar and colour multiplication.</p>
<p><i>Quick note on spelling</i>. I'm from New Zealand where we use mostly English spelling for words like "colour". However in my code I use American spelling because I feel that is the <i>de factor</i> worldwide standard for programming.</p>
<p>One last thing to do before computing some shading here is to actually place a light in the scene.  We'll also set the scene's ambient light. In this code the Light type is new, the Scene and Sphere types are modified.</p>
<p>{% highlight csharp %}

type Sphere = { center:Point3D; radius:float; diffuseColor:Color }
type Light  = { position:Point3D; color:Color }
type Scene  = { camera:Camera; sphere:Sphere; ambientLight:Color; light:Light }

{% endhighlight %}
</p>
<p>Also modify our scene declaration a bit<br />
{% highlight csharp %}

    // scene
    let light = { position=Point3D(0.0,3.0,4.0); color=Color(0.8,0.8,0.8) }
    let scene = { camera=camera; sphere=sphere; ambientLight=Color(0.2,0.2,0.2); light=light }

{% endhighlight %}
</p>
<p>Okay, let's do some light computation. We'll start with the simplest one - ambient light. Ambient light is given by the expression<br />
{% raw %}
$$ I = {k_d}{I_a}, ~ 0 <= k_d <=1 $$
{% endraw %}<br />
where \( I_a \) is the intensity of our ambient lighting in the scene and \( k_d \) is the diffuse reflectivity of the sphere's material. Sometimes people model ambient light with \( k_a \), giving objects a different diffuse and ambient reflectivity. In my opinion this case is rare enough that we can skip it for now.<br />
{% highlight csharp %}

let colorAt intersections scene =
    let closest = List.maxBy(fun i -> i.t) intersections
    let kd = closest.sphere.diffuseColor
    let Ia = scene.ambientLight
    Ia * kd

{% endhighlight %}
</p>
<p>And a small modification to the nested for-loop to make this all work.<br />
{% highlight csharp %}

    for x in 0..(width-1) do
        for y in 0..(height-1) do
            let rayPoint = vpc + float(x-width/2)*pw*u + float(y-height/2)*ph*v
            let rayDir = norm (rayPoint - scene.camera.position)
            let ray = { origin = scene.camera.position; direction = rayDir }
            let intersects = castRay ray scene
            match intersects with
            | [] -> bmp.SetPixel(x,y,Color.Gray)
            | _ -> let color = colorAt intersects scene
                   bmp.SetPixel(x,y, Color.FromArgb(255, (int)(color.r*255.0), (int)(color.g*255.0), (int)(color.b*255.0)))

{% endhighlight %}
<br />
<a href="/images/2011-02-fray-output-ambient.png"><img src="/images/2011-02-fray-output-ambient.png" alt="" title="fray-output-ambient" width="288" height="192" class="alignright size-full wp-image-511" /></a><br />
We should really create a new function to do the transformation between our Color type and the built-in one, but for now we'll leave it. Running the program now results in the image on the right. This is the sphere lit only by our 20% ambient light.</p>
<p>Now for diffuse lighting. The diffuse portion is the diffuse colour of the sphere multiplied by the projection of a vector from the intersection point to the light source (normalized) on the normal vector (normalized) of the intersection point. In other words, the dot product of these two vectors. Mathematically, diffuse lighting is given by<br />
{% raw %}
$$ I = {k_d}(L . N)I_d $$
{% endraw %}<br />
where \( k_d \) is the material's diffuse reflectivity (as before), L is the vector between the light source and the intersection point (light.position - intersection.point), N is the normal of the intersection point (both normalized) and \( I_d \) is the intensity of the light. (That's a dot product between L and N, amazingly my maths library doesn't seem to have a dot product character!).</p>
<p>So we add diffuse reflection and ambient together:<br />
{% highlight csharp %}

let colorAt intersections scene =
    let closest = List.maxBy(fun i -> i.t) intersections
    let kd = closest.sphere.diffuseColor
    let Ia = scene.ambientLight
    let Id = Math.Max(0.0, Vector3D.DotProduct(norm (scene.light.position - closest.point), norm closest.normal))
    let ambient = kd * Ia
    let diffuse = kd * Id
    ambient + diffuse

{% endhighlight %}
<br />
<a href="/images/2011-02-fray-output-ambient-and-diffuse1.png"><img src="/images/2011-02-fray-output-ambient-and-diffuse1.png" alt="" title="fray-output-ambient-and-diffuse" width="288" height="192" class="alignleft size-full wp-image-540" /></a>We're getting there! See the image on the left. I'd like to take a moment to explain that List.maxBy call above, because it is quintessential functional programming. If you have never done functional programming before it probably looks a bit strange. Something to understand about this paradigm is that functions are <a href="http://en.wikipedia.org/wiki/First-class_object">first-class objects</a>. This means that functions can be passed around as parameters and returned from other functions. Essentially we can treat functions as though they were data members in functional languages. </p>
<p>List.maxBy is a function that takes two parameters - the first parameter is <i>another function</i> which takes some object of the list type (T') and returns some other object of type U' with which we make the comparison. The second parameter is a list of type T' and the return type is the T' which has the maximum value of all of the U's associated with the T's in the list. In this case T' is type Intersection and U' is type float (the <i>t</i> value of our intersection). So we're asking the List.maxBy function to give us the Intersection in this list which has the highest <i>t</i> value. Understand? It's a little tricky if you've never seen it before, but it's a very powerful technique.</p>
<p>All we have left to compute is the specular component and we have a complete lighting model. Specular light can be given as<br />
{% raw %}
$$ I = {k_s}{(N . H)^alpha}I_s $$
{% endraw %}<br />
where \( k_s \) is the specular reflectivity of the material (the same as ambient/diffuse reflectivity earlier, but usually a white colour), N is the surface normal at intersection (normalized), \( I_s \) is the specular intensity of the light source, \( alpha \) is a material constant (usually on the order or 100-500) and H is a vector called the "half-way vector". H is the vector half way between the view vector (camera.position - intersection.point) and the light vector (light.position - intersection.point). I won't go into detail about the geometry that makes this work because it has been covered all over the place (see <a href="http://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model">Blinn-Phong shading model</a>), but suffice it to say that this expression will give us nice-looking specular highlights :) You can vary the value of \( alpha \) to change the size and sharpness of the specular highlight - this value is often referred to as the "shininess" value of a material. So here is the modified code to include all three shading modes.</p>
<p>{% highlight csharp %}

let colorAt intersections scene =
    let closest = List.maxBy(fun i -> i.t) intersections
    let kd = closest.sphere.diffuseColor
    let Ia = scene.ambientLight
    let L = norm (scene.light.position - closest.point)
    let Id = Math.Max(0.0, Vector3D.DotProduct(L, closest.normal))
    let V = scene.camera.position - closest.point
    let H = norm (L + V)
    let Is = Math.Pow(Math.Max(0.0, Vector3D.DotProduct(H,closest.normal)), 500.0)
    let ambient = kd * Ia
    let diffuse = kd * Id
    let specular = Color(1.0,1.0,1.0) * Is
    ambient + diffuse + specular

{% endhighlight %}
</p>
<p><a href="/images/2011-02-fray-output-full.png"><img src="/images/2011-02-fray-output-full.png" alt="" title="fray-output-full" width="288" height="192" class="alignleft size-full wp-image-538" /></a>This is a little messy, but we'll tidy up later! I have actually cheated here and just used pure white light for \( I_s \). We will implement a material type in the next article.</p>
<p>Notice how when we add all of these shading methods together we get the expression<br />
{% raw %}
$$ I = {k_a}{I_a} + {k_d}(L . N)I_d + {k_s}{(N . H)^alpha}I_s $$
{% endraw %}<br />
When dealing with multiple light sources, this will become<br />
{% raw %}
$$ I = {k_a}{I_a} + sum{m subset lights}{}({k_d}(L_m . N)I_dm + {k_s}{(N . H)^alpha}I_sm) $$
{% endraw %}<br />
Which is the Phong shading model! (modified to use the Blinn-Phong specular method). We will implement this multiplicity (of lights and shapes) in the <a href="http://blog.martindoms.com/2011/02/22/ray-tracer-in-f-part-iii/">next article</i> (now available). Stay tuned!</p>
<p><a href="http://martindoms.com/scratch/FRay02.fs">Source code so far</a>.</p>
<script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>